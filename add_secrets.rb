#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'byebug'
require 'yaml'

secrets = YAML.load_file('secrets.yml')
proj_id = '<PROJECT_ID>'
pat = '<GITLAB_PAT>'

url = "https://gitlab.com/api/v4/projects/#{proj_id}/variables"

def create_key(key)
  env = 'MASTER'
  "K8S_SECRET_#{env}_#{key}"
end

result = secrets.map do |key, value|
  k8s_key = create_key(key)
  puts "Adding key: #{key} (k8s key: #{k8s_key})"
  HTTParty.post(url,
                query: { key: k8s_key, value: value },
                headers: { 'Content-Type' => 'application/json',
                           'Accept' => 'application/json',
                           'PRIVATE-TOKEN' => pat })
end

puts result
